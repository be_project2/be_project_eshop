# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

# LEROYYY

import scrapy


class ProductItem(scrapy.Item):

    zone = scrapy.Field()
    category = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    description = scrapy.Field()

    pass
