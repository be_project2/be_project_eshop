
import random as rng
import csv
import re

# products csv for presta:
# ID, Categories (x,y,z...), Name, Quantity, Price tax excluded, Price tax included, Cost price, Base price, Discount amount,
#           Discount percent, Discount from (yyyy-mm-dd), Discount to (yyyy-mm-dd), DescriptionImage URLs (x,y,z...)
PRODUCTS_HEAD = [
    "ID",
    "Categories (x,y,z...)",
    "Name",
    "Quantity",
    "Price tax excluded",
    "Price tax included",
    "Cost price",
    "Base price",
    "Discount amount",
    "Discount percent",
    "Discount from (yyyy-mm-dd)",
    "Discount to (yyyy-mm-dd)",
    "Description",
    "Image URLs (x,y,z...)"
]

# categories csv for presta:
# Category ID, Name, Parent category, Root category (0/1), Description, Meta title, Image URLs (x,y,z...)
CATEGORIES_HEAD = [
    "Category ID",
    "Name",
    "Parent category",
    "Root category (0/1)",
    "Description",
    "Meta title",
    "Image URLs (x,y,z...)"
]

HTTP_SITE_ADRESS = "http://127.0.0.1:8080"

CSV_PRODUCTS_INPUT_NAME = "productsFile.csv"
CSV_PRODUCTS_OUTPUT_NAME = "prestaCsvProducts.csv"
CSV_CATEGORIES_OUTPUT_NAME = "prestaCsvCategories.csv"
CSV_IMAGES_LOCAL_STORAGE = False

PRODUCT_IMAGE_HEAD = "images/"
PRODUCT_IMAGE_TAIL = ".jpg"

CHAR_QUOTE = "'"
CHAR_DASH = "-"
CHAR_COMMA = ","
CHAR_PERIOD = "."
CHAR_US = "_"

QUANTITY_MIN = 15
QUANTITY_MAX = 20000

TAX_VALUE = 23  # in %

# all in %
DISCOUNT_AM_CHANCE = 20
DISCOUNT_PR_CHANCE = 20
DISCOUNT_MIN = 5
DISCOUNT_MAX = 20
DISCOUNT_MAX_NUMBER = 40
DISCOUNT_FROM_YEAR = "2022"
DISCOUNT_TO_YEAR = "2023"


ONLY_ALLOWED_ZONES = True
ALLOWED_ZONES = [
    "women",
    "men"
]

NA_STRING = "N/A"
ACCEPT_NA = False

PRODUCT_REFERENCE = "scraper item"
PRODUCT_STATUS = 0

CATEGORY_DISPLAYED = 1
CATEGORY_DESCRIPTION = "placeholder category description"
CATEGORY_HOME = "Home"
CATEGORY_ROOT_CAT = 0
CATEGORY_META = "Meta title-for-"
CATEGORY_START_ID = 3

def csvToList(fileName):
    list = []
    file = open(fileName, 'r', newline='', encoding="utf8")
    reader = csv.reader(file)
    for row in reader:
        list.append(row)

    return list

def saveToCsv(list, fileName):
    file = open(fileName, 'w', newline='')
    writer = csv.writer(file, delimiter=';')
    for item in list:
        writer.writerow(item)
    return

def extractImgUrl(inpStr, index):
    outputStr = ""
    urlIndex = 0
    for char in inpStr:
        if char == CHAR_COMMA:
            if urlIndex == index:
                break
            else:
                urlIndex += 1
        else:
            if urlIndex == index:
                outputStr += char
            else:
                continue
    return outputStr

def removeInColor(productName):
    productName = re.sub(r' in.*', '', productName)
    return productName

# format for image path is: images/{product name}_{image index}.jpg
# only if CSV_IMAGES_LOCAL_STORAGE is set to True
def extractImgLocalPaths(product):
    productName = product[3].replace(" ", "_").replace("/", "_")
    productImages = product[8]

    outputStr = ""

    if productImages == "":
        return ""

    imageURLs = 1
    for char in productImages:      # counting images available for product
        if char == CHAR_COMMA:
            imageURLs += 1

    for index in range(imageURLs):
        imagePath = PRODUCT_IMAGE_HEAD + productName + CHAR_US + str(index) + PRODUCT_IMAGE_TAIL
        outputStr += imagePath + CHAR_COMMA

    return outputStr[:-1]   # [:-1] to discard the last COMMA (,) sign in the string

def findCategoryID(zoneName, categoryName, categoriesOutput):
    foundID = -1
    for category in categoriesOutput:
        if categoryName == category[1]:
            if zoneName == category[2]:
                foundID = category[0]

    return foundID

def validRecord(product):
    zone = product[0]
    category = product[1]
    type = product[2]
    name = product[3]

    if not ACCEPT_NA and (zone == NA_STRING or category == NA_STRING or name == NA_STRING):
        return False
    if ONLY_ALLOWED_ZONES and zone not in ALLOWED_ZONES:
        return False
    return True

def main():

    products = csvToList(CSV_PRODUCTS_INPUT_NAME)
    productsOutput = []
    categoriesOutput = []
    categoryNames = []
    zoneNames = []
    productID = 1
    categoryID = CATEGORY_START_ID
    discountsGiven = 0

    categoriesOutput.append(CATEGORIES_HEAD)

    ## ZONES ## looking for all zones in productsFile
    for product in products[1:]:

        # checking validity (mostly N/A records) accordingly to setup above
        if not validRecord(product):
            continue

        zone = product[0]

        # change setting above to convert images to local paths instead of url links to bale site
        if CSV_IMAGES_LOCAL_STORAGE:
            product[8] = extractImgLocalPaths(product)

        if zone not in zoneNames:
            zoneNames.append(zone)
            row = [
                categoryID,                     # Category ID
                zone,                           # Name
                CATEGORY_HOME,                  # Parent category
                CATEGORY_ROOT_CAT,              # Root category (0/1)
                CHAR_QUOTE + zone + "'s category" + CHAR_QUOTE,     # Description
                CATEGORY_META + zone,           # Meta title
                extractImgUrl(product[8], 0)    # Image URLs (x,y,z...)
            ]
            categoryID += 1
            categoryNames.append(zone)
            categoriesOutput.append(row)

    ## CATEGORIES ## looking for all categories in productsFile
    for product in products[1:]:

        # checking validity (mostly N/A records) accordingly to setup above
        if not validRecord(product):
            continue

        zone = product[0]
        category = product[1]
        if findCategoryID(zone, category, categoriesOutput) == -1:
            row = [
                categoryID,                                     # Category ID
                category,                                       # Name
                zone,                                           # Parent category
                CATEGORY_ROOT_CAT,                              # Root category (0/1)
                CHAR_QUOTE + category + " for " + zone + CHAR_QUOTE,  # Description
                CATEGORY_META + category + CHAR_DASH + zone,    # Meta title
                extractImgUrl(product[8], 0)                    # Image URLs (x,y,z...)
            ]
            categoryID += 1
            categoryNames.append(category)
            categoriesOutput.append(row)


    ## PRODUCTS ## saving products with prestashop like format

    productsOutput.append(PRODUCTS_HEAD)

    for product in products[1:]:
        if not validRecord(product):
            continue

        zone =  product[0]
        category = product[1]
        type = product[2]
        name = product[3]
        price = product[4]
        description = product[5]
        details1 = product[6]
        details2 = product[7]
        image_urls = product[8]

        name = removeInColor(name)

        categorysID = findCategoryID(zone, category, categoriesOutput)

        quantity = rng.randint(QUANTITY_MIN, QUANTITY_MAX)

        # PRICES #
        priceTaxEx = int(price.replace(' ', ''))            # replace() to remove whitespaces from price string
        priceTaxIn = priceTaxEx * ((100 + TAX_VALUE)/100)
        costPrice = priceTaxEx
        basePrice = priceTaxEx

        # DICOUNT #
        discountAmount = ""
        discountPercent = ""
        if discountsGiven < DISCOUNT_MAX_NUMBER:
            discountRng = rng.randint(0, 100)
            if discountRng < DISCOUNT_PR_CHANCE:
                discountPercent = rng.randint(DISCOUNT_MIN, DISCOUNT_MAX)
                discountsGiven += 1
                print("discounts Given: " + str(discountsGiven))
            if DISCOUNT_PR_CHANCE < discountRng < DISCOUNT_AM_CHANCE + DISCOUNT_PR_CHANCE:
                tmp = rng.randint(DISCOUNT_MIN, DISCOUNT_MAX) * priceTaxIn / 100
                discountAmount = round(tmp) - round(tmp) % 10
                discountsGiven += 1
                print("discounts Given: " + str(discountsGiven))

        # DISCOUNT DATE #
        discountStartMonth = rng.randint(1, 12)
        discountEndMonth = rng.randint(1, 12)
        discountStartDay = rng.randint(1, 30)
        discountEndDay = rng.randint(1, 30)
        discountFrom = DISCOUNT_FROM_YEAR + CHAR_DASH + str(discountStartMonth) + CHAR_DASH + str(discountStartDay)
        discountTo = DISCOUNT_TO_YEAR + CHAR_DASH + str(discountEndMonth) + CHAR_DASH + str(discountEndDay)


        row = [
            productID,
            categorysID,
            name,
            quantity,
            priceTaxEx,
            priceTaxIn,
            costPrice,
            basePrice,
            discountAmount,
            discountPercent,
            discountFrom,
            discountTo,
            description,
            image_urls
        ]
        productID += 1
        productsOutput.append(row)


    saveToCsv(productsOutput, CSV_PRODUCTS_OUTPUT_NAME)
    saveToCsv(categoriesOutput, CSV_CATEGORIES_OUTPUT_NAME)

    return 0


main()