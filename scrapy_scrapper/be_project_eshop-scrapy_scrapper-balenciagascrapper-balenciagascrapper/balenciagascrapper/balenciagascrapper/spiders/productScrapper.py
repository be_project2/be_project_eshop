
from scrapy import spiders
from balenciagascrapper import config as cfg


class ProductScrapper(spiders.Spider):

    name = "productScrapper"
    allowed_domains = [cfg.ALLOWED_DOMAIN]
    start_urls = [cfg.URL_MAIN]
    globalProductId = 1

    def parse(self, response):

        # loops for every desired zone and category in order specified in config.py
        for zone in cfg.FOLLOW_TREE:
            zoneName = zone["zoneName"]
            for categoryName in zone["categoryNames"]:

                categoryURL = cfg.URL_BASE + zoneName + str('/') + categoryName + "?sz=" + str(cfg.NR_OF_PRODUCTS_FROM_CATEGORY)

                yield response.follow(categoryURL, callback=self.parse_category)


    # opening each product page from given category (from url response)
    def parse_category(self, response):

        products = response.css('div.c-product__item')

        for product in products:
            productLink = product.css('a.c-product__focus::attr(href)').get()
            yield response.follow(productLink, callback=self.parse_product)
            self.globalProductId += 1


    # gathering all data for given product (from url response)
    def parse_product(self, response):

        # ZONE / CATEGORY / TYPE from product page
        # try getting data, if n/a then put n/a string or return with no item
        origin = response.css('a.c-breadcrumbs__link span::text').getall()
        try:
            zoneName = origin[0].strip().casefold()
        except:
            if cfg.INCLUDE_ONLY_FULL_DATA_ITEMS: yield
            zoneName = cfg.NA_STRING
        try:
            categoryName = origin[1].strip().casefold()
        except:
            if cfg.INCLUDE_ONLY_FULL_DATA_ITEMS: yield
            categoryName = cfg.NA_STRING
        try:
            typeName = origin[2].strip().casefold()
        except:
            if cfg.INCLUDE_ONLY_FULL_DATA_ITEMS: yield
            typeName = cfg.NA_STRING


        # detail info 1 is from html element with <br> tags which requires
        # first extracting and then removing unnecessary symbols
        tmp_details1 = []
        for detail in response.css('div.c-product__detailinfo::text').extract():
            detail = detail.strip(" \n\u2022")
            tmp_details1.append(detail)

        tmp_details2 = response.css('ul.c-product__detailinfo li::text').get().strip().replace('\n', '').replace('  ', '')

        # yield work like return but for scrapy framework
        yield {
            'zone': zoneName,
            'category': categoryName,
            'type': typeName,
            'name': response.css('h1.c-product__name::text').get().strip(),
            'price': response.css('p.c-price__value--current::text').get().strip().replace(' €', ''),
            'description' : response.css('p.c-product__longdesc::text').get().strip(),
            'details1': tmp_details1,
            'details2': tmp_details2,
            'image_urls': response.css('img.c-product__image::attr(data-src)').getall()
        }

