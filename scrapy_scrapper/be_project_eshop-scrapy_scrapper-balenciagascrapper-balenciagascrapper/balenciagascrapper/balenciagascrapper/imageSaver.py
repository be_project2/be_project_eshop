
import csv
import urllib.request

# saving format for images is: images/{product name}_{image index}.jpg

CSV_PRODUCTS_INPUT_NAME = "productsFile.csv"
CHAR_COMMA = ","
IMAGE_COUNT_FOR_PRODUCT_MAX = 3
IMAGE_SAVE_PATH_HEAD = "images/"
IMAGE_SAVE_PATH_TAIL = ".jpg"

def csvToList(fileName):
    list = []
    file = open(fileName, 'r', newline='', encoding="utf8")
    reader = csv.reader(file)
    for row in reader:
        list.append(row)

    return list

def extractImgUrl(inpStr, index):
    outputStr = ""
    urlIndex = 0
    for char in inpStr:
        if char == CHAR_COMMA:
            if urlIndex == index:
                break
            else:
                urlIndex += 1
        else:
            if urlIndex == index:
                outputStr += char
            else:
                continue
    return outputStr

def main():

    stat_pr_count = 0
    products = csvToList(CSV_PRODUCTS_INPUT_NAME)

    for product in products[1:]:
        print("product: ")
        for index in range(IMAGE_COUNT_FOR_PRODUCT_MAX):
            imageURL = extractImgUrl(product[8], index)
            if imageURL == "":
                break

            print(imageURL)

            productName = product[3]
            productName = productName.replace(" ", "_").replace("/", "_")

            imageName = productName + "_" + str(index)

            imagePath = IMAGE_SAVE_PATH_HEAD + imageName + IMAGE_SAVE_PATH_TAIL

            print(imagePath)

            urllib.request.urlretrieve(imageURL, imagePath)
            stat_pr_count += 1

    print("retrieved: ")
    print(stat_pr_count)

main()












