
URL_MAIN = "https://www.balenciaga.com/en-pl"
ALLOWED_DOMAIN = "balenciaga.com"
URL_BASE = "https://www.balenciaga.com/en-pl/"
NA_STRING = 'N/A'

INCLUDE_ONLY_FULL_DATA_ITEMS = False
NR_OF_PRODUCTS_FROM_CATEGORY = 60

FOLLOW_TREE = [
    {
        'zoneName': 'women',
        'categoryNames': [
            'ready-to-wear',
            'shoes',
            'bags',
            'accessories'
        ]
    },
    {
        'zoneName': 'men',
        'categoryNames': [
            'ready-to-wear',
            'shoes',
            'bags',
            'accessories'
        ]
    }
]